### Generic Intel with PEXSI, AG version###

SET(CMAKE_Fortran_FLAGS "-O3 -fp-model precise" CACHE STRING "Fortran flags")
SET(CMAKE_C_FLAGS "-O3 -fp-model precise -std=c99" CACHE STRING "C flags")
SET(CMAKE_CXX_FLAGS "-O3 -fp-model precise -std=c++11" CACHE STRING "C++ flags")


SET(ENABLE_TESTS OFF CACHE BOOL "Enable Fortran tests")
SET(ENABLE_C_TESTS OFF CACHE BOOL "Enable C tests")
SET(ENABLE_PEXSI ON CACHE BOOL "Enable PEXSI")
SET(ADD_UNDERSCORE ON CACHE BOOL "Add underscore")
SET(ELPA2_KERNEL "AVX" CACHE STRING "Use ELPA AVX kernel")

SET(LIB_PATHS "$ENV{MKLROOT}/lib/intel64" CACHE STRING "External library paths")
SET(LIBS "mkl_scalapack_lp64 mkl_blacs_intelmpi_lp64 mkl_intel_lp64 mkl_sequential mkl_core" CACHE STRING "External libraries")

#SET(MATH_LIB "/opt/scalapack/openmpi-2.1.2--gfortran-7.2.0/lib/libscalapack.a;-lveclibfort" CACHE STRING "Linear algebra libraries")
#SET(MATH_LIB "$ENV{SCALAPACK_LIBS};$ENV{LAPACK_LIBS}" CACHE STRING "Linear algebra libraries")
