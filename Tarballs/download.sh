#!/bin/bash
set -ex

wget https://gitlab.com/garalb/siesta/-/archive/rel-MaX-1/siesta-rel-MaX-1.tar.gz
wget https://icmab.es/leem/SIESTA_MATERIAL/tarballs/libxc-4.2.3.tar.gz
wget https://icmab.es/leem/SIESTA_MATERIAL/tarballs/flook-0.7.0-23.tar.gz
wget https://launchpad.net/xmlf90/trunk/1.5/+download/xmlf90-1.5.4.tar.gz
wget https://launchpad.net/libpsml/trunk/1.1/+download/libpsml-1.1.8.tar.gz
wget https://launchpad.net/libgridxc/trunk/0.8/+download/libgridxc-0.8.5.tgz
wget http://wordpress.elsi-interchange.org/wp-content/uploads/2019/11/elsi-2.4.1.tar.gz
